*** Settings ***
Library   REST   url=https://www.onnistuu.fi
Library  Collections

*** Variables ***
${HOST}   https://www.vismasign.fi
&{AUTHOR_HEADER}         

*** Keywords ***
Base64 Encode String
    [Arguments]   ${string_to_encode}
    ${encoded_string}=    Convert To Bytes    ${string_to_encode}
    ${encoded_string}=    Evaluate    base64.b64encode($encoded_string)    base64
    [Return]  ${encoded_string}

Get Authorization Header
    [Arguments]   ${client_id}   ${secret}
    ${64_encode}   Base64 Encode String      ${secret}
    &{headers}  Create Dictionary  Authorization=Onnistuu ${client_id}:${64_encode}
    Set To Dictionary   ${headers}    Content-Type=application/json
    Set To Dictionary   ${headers}    Content-MD5=1B2M2Y8AsgTpgAmY7PhCfg==
    Set To Dictionary   ${headers}    Host=${HOST}
    Set To Dictionary   ${headers}    Connection=keep-alive
    #todo: get correct date
    Set To Dictionary   ${headers}    Date    Wed, 21 Oct 2020 11:07:28 GMT
    [Return]   ${headers}

Get Invitation Status
   [Arguments]   ${uuid}=${EMPTY}   ${header}=${EMPTY}
   ${json_string}=    evaluate    json.dumps(${header})    json
   GET  /api/v1/invitation/${uuid}     #headers=${json_string}

Generate Authorization Header
   [Arguments]   ${client_id}   ${secret}
   &{header}   Get Authorization Header  ${client_id}  ${secret}
   Set To Dictionary    ${AUTHOR_HEADER}      ${header}

