# VismaSign Robot

Robot Framework resources and keywords for Visma Sign automated testing

Directory structure
```
*--
  |
  +- resources
        *vismasign_api.robot
        *vismasign.robot
  +- lib
        <placeholder>
  +- keyword_tests
        keyword_tests.robot
        
```

*Installation*
-----------------

Install python 3.7:

To install Homebrew, open Terminal or your favorite OS X terminal emulator and run
```
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```
The script will explain what changes it will make and prompt you before the installation begins. Once you’ve installed Homebrew, <br>
insert the Homebrew directory at the top of your PATH environment variable. 
<br>You can do this by adding the following line at the bottom of your ~/.profile file
```
export PATH="/usr/local/opt/python/libexec/bin:$PATH"
```

Now, we can install Python 3:
```
$ brew install python
```
This will take a minute or two.


install and upgrade from PyPi:
```
pip install virtualenv 
python3 -m venv venv
source venv/bin/activate
pip install --upgrade RESTinstance
#run tests:
robot .
```

run keyword test from repository root:
```
#run tests:
robot .
```
